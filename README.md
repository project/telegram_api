# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Add some telegram integration for your site.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/telegram_api>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/telegram_api>


## REQUIREMENTS

php curl library (ext-curl)


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

There is no any configuration now.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>

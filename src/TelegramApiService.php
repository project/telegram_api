<?php

namespace Drupal\telegram_api;

/**
 * Telegram API Service.
 */
class TelegramApiService {

  /**
   * Send to telegram method.
   */
  public function sendToTelegramBot(string $text, string $token, string $chatid, bool $proxy = FALSE, ?string $proxy_server = NULL, ?string $proxy_login = NULL, ?string $proxy_pass = NULL): bool|string {
    $curl = curl_init();

    if ($curl) {
      $query = "https://api.telegram.org/bot" . $token .
        "/sendMessage?disable_web_page_preview=true&chat_id=" . $chatid .
        "&text=" . urlencode($text);

      curl_setopt($curl, CURLOPT_URL, $query);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_POST, TRUE);
      if ($proxy) {
        curl_setopt($curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
        curl_setopt($curl, CURLOPT_PROXY, $proxy_server);
        if ($proxy_login) {
          curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxy_login . ':' . $proxy_pass);
        }
      }

      if (curl_exec($curl) === FALSE) {
        return curl_error($curl);
      }

      curl_close($curl);
      return TRUE;
    }

    return 'Cannot initialize cURL. Is it installed on the server?';
  }

}
